New IC corporate website based on Nuxt.js

Features

- i18n
- sass-loader
- bootstrap 4 (tree shaked)
- frontmatter-markdown-loader
- forestry-preview
- axios