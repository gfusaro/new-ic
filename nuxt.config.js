import path from "path";
/* eslint-disable */
const glob = require('glob');
const config = require("./content/data/config.json")
/* eslin-enable */
const dynamicRoutes = getDynamicPaths({
  '/blog': 'content/blog-posts/*.md',
 });

export default {
  mode: 'universal',
  /*
   ** Headers of the page
   */
  head: {
    title: config.title || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: config.description || ''
      }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
  {
    href: "https://fonts.googleapis.com/css?family=Maven+Pro|Noto+Sans&display=auto",
    rel:"stylesheet"
  }]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: {
    color: '##02aea9'
  },
  /*
   ** Global CSS
   */
  css: ['@/assets/styles/global.scss'],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: ['~/plugins/i18n.js', '~/plugins/axios'],
  /*
   ** Middlewares
   */
  router: {
    middleware: 'i18n'
  },
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module'
  ],
  /*
   ** Nuxt.js modules
   */
  // modules:
  //   ['bootstrap-vue/nuxt','@nuxtjs/pwa', '@nuxtjs/axios', '@nuxtjs/svg'],
  //   bootstrapVue: {
  //     components: ['BContainer', 'BRow', 'BCol'],
  //     directives: []
  // },
  modules: ['@nuxtjs/pwa', '@nuxtjs/axios', '@nuxtjs/svg', '@nuxtjs/svg-sprite'],
    svgSprite: {
      input: '~/static/svg/'
    },
  /*
   ** Progressive web app (manifest)
   */
  pwa: {
    cacheOptions: {
      cacheId: '<npm package name> || nuxt',
      directoryIndex: '/',
      revision: undefined
    },
    offline: true,
    manifest: {
      name: 'ic',
      lang: 'it',
      background_color: '#02aea9',
      theme_color: "#02aea9",
      icons: [{
          src: "/static/icons/icon-72x72.png",
          sizes: "72x72",
          type: "image/png"
        },
        {
          src: "/static/icons/icon-96x96.png",
          sizes: "96x96",
          type: "image/png"
        },
        {
          src: "/static/icons/icon-128x128.png",
          sizes: "128x128",
          type: "image/png"
        },
        {
          src: "static/icons/icon-144x144.png",
          sizes: "144x144",
          type: "image/png"
        },
        {
          src: "/static/icons/icon-152x152.png",
          sizes: "152x152",
          type: "image/png"
        },
        {
          src: "/static/icons/icon-192x192.png",
          sizes: "192x192",
          type: "image/png"
        },
        {
          src: "/static/icons/icon-384x384.png",
          sizes: "384x384",
          type: "image/png"
        },
        {
          src: "/static/icons/icon-512x512.png",
          sizes: "512x512",
          type: "image/png"
        }
      ],
    }
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** Using frontmatter-markdown-loader here to parse md files
     */
    extend(config, ctx) {  
      config.module.rules.push(
      {
          test: /\.md$/,
          loader: "frontmatter-markdown-loader",
          include: path.resolve(__dirname, "content/")
      })
    }    
  },
  generate: {
    routes: dynamicRoutes
  },
  vendor: ['i18n', 'axios', 'svg'],
}
/**
 * Create an array of URLs from a list of files
 * @param {*} urlFilepathTable
 */

/* referenced https://github.com/jake-101/bael-template */
function getDynamicPaths(urlFilepathTable) {
  return [].concat(
    ...Object.keys(urlFilepathTable).map(url => {
      const filepathGlob = urlFilepathTable[url];
      const routes = glob
        .sync(filepathGlob)
        .map(filepath => `${url}/${path.basename(filepath, '.md')}`);
      return routes
    })
  );
}
